#ifndef GRID_H
#define GRID_H

#include <vector>   //vector
#include <utility>  //pair
#include "tile.h"
#include "score.h"

class Grid
{
private:
   std::vector< std::vector<Tile> > m_matrix;
   Score m_score;

   Tile getRandomAvailableTile();
   bool moveTile(unsigned i, unsigned j, int x, int y, bool & fusion);
public:
    Grid();
    Grid(std::vector< std::vector<Tile> > & matrix, Score score);
    void addRandomTile();
    bool isFull();
    bool isFusionnable();
    bool move(char direction) throw();
    QString getMatrixStr();

//debug:
    void printDebug();
    void addTile(unsigned x, unsigned y, unsigned value = 2);
    void empty();
};

#endif // GRID_H
