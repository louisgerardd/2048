#include "mainwindow.h"
#include <QApplication>
#include <QFileSystemWatcher>
#include <QTimer>
#include <QDebug>
#include <time.h>   //time(), srand()
#include <vector>

int main(int argc, char *argv[])
{
    srand(time(NULL));
    QApplication a(argc, argv);

    std::vector< std::vector<Tile> > grid1;
    std::vector< std::vector<Tile> > grid2;

    MainWindow w;
    w.show();

    //Modif fichier
    QFileSystemWatcher fileWatcher1;
    fileWatcher1.addPath("player1.move");
    QObject::connect(&fileWatcher1, SIGNAL(fileChanged(const QString &)), &w, SLOT(fileChangedSlot(const QString &)));

    QFileSystemWatcher fileWatcher2;
    fileWatcher2.addPath("player2.move");
    QObject::connect(&fileWatcher2, SIGNAL(fileChanged(const QString &)), &w, SLOT(fileChangedSlot(const QString &)));

    //Timer
    QTimer timer;
    timer.setSingleShot(false);
    timer.setInterval(w.getPauseTime());
    QObject::connect(&timer, SIGNAL(timeout()), &w, SLOT(timerSlot()));
    timer.start();

    return a.exec();
}
