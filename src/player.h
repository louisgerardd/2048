#ifndef PLAYER_H
#define PLAYER_H

#include <QLabel>
#include <QStatusBar>
#include "grid.h"

class Player
{
public:
    Player();
    void initialize(Grid & grid, QString file, QLabel* & title, QStatusBar* bar);
    bool play(char direction);

private:
    Grid m_grid;
    QString m_file;
    QLabel *m_title;
    QStatusBar *m_bar;
    void writeReturn() throw();
};

#endif // PLAYER_H
