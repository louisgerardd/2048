#include "player.h"

#include <QDebug>
#include <QFile>
#include <QMessageBox>

Player::Player() {}

void Player::initialize(Grid & grid, QString file, QLabel *&title, QStatusBar * bar)
{
    m_grid = grid;
    m_file = file;
    m_title = title;
    m_bar = bar;
    writeReturn();
}

bool Player::play(char direction)
{
    try
    {
        if(m_grid.move(direction))
        {
            m_grid.addRandomTile();
            m_bar->showMessage("Joueur 1 : " + QString(direction));
        }
        else if(m_grid.isFull() && !m_grid.isFusionnable())
        {
            m_title->setText("Lost !");
            return true;
        }
        else
            m_bar->showMessage("Joueur 1 : Faux mouvement !");
        qDebug() << "direction : " << QChar(direction) << "\n";
        m_grid.printDebug();
        qDebug() << "\n";
    }
    catch(QString e)
    {
        m_bar->showMessage(e);
    }

    writeReturn();
    return false;
}

void Player::writeReturn() throw()
{
    QFile matrixFile(m_file);
    if (!matrixFile.open(QIODevice::WriteOnly))
    {
        throw "Nous n'avons pas réussi à lire les informations.";
        std::exit(1);
    }
    QTextStream outStream(&matrixFile);
    outStream << m_grid.getMatrixStr();
    matrixFile.close();
}
