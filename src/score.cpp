#include "score.h"

void Score::add(unsigned points)
{
    m_value += points;
    m_uiObject->setText(QString::number(m_value));
}

void Score::linkUI(QLabel* & uiObject)
{
    m_uiObject = uiObject;
}

Score::Score() {}

