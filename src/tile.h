#ifndef TILE_H
#define TILE_H

#include <QLabel>

class Tile
{
private:
    unsigned m_value;
    QLabel *m_uiObject;
    const static unsigned COLORS[];

public:
    Tile(QLabel* & uiObject);
    void setValue(unsigned value);
    unsigned getValue();
    bool operator == (Tile t2);
    bool operator == (unsigned val2);
};

#endif // TILE_H
